<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
?>

<a href="#" class="d-flex user position-relative" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	<img class="profile" alt="profile" src="<? echo CFile::GetPath($arUser["PERSONAL_PHOTO"]); ?>" />
	<div class="name"><? echo $arUser["NAME"].' '.$arUser["LAST_NAME"];?></div>
</a>