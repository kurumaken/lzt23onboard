<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
	die();
}
CModule::IncludeModule("iblock");

if ($_POST) {
	// echo '<pre>';
	// print_r($_POST);
	// echo '</pre>';
	// Удалить все цели пользователя
	$arSelect = Array("ID", "NAME");
	$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "PROPERTY_USER"=>$_GET["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>100), $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		// echo '<pre>';
		// print_r($arFields);
		// echo '</pre>';
		CIBlockElement::Delete($arFields["ID"]);
	}
	// Создать цели для пользователя
	foreach($_POST as $k=>$v) {
		// echo '<pre>';
		// print_r($v);
		// echo '</pre>';
		if ($v) {
			
			$el = new CIBlockElement;
			$PROP = array();
			$PROP["USER"] = $_GET["ID"];
			
			$arLoadProductArray = Array(
				"MODIFIED_BY" => $USER->GetID(), 				
				"IBLOCK_ID" => 2,
				"NAME" => $v,
				"PROPERTY_VALUES"=> $PROP,
				// "IBLOCK_SECTION_ID" => false,				
				// "ACTIVE"         => "Y",
				// "PREVIEW_TEXT"   => "текст для списка элементов",
				// "DETAIL_TEXT"    => "текст для детального просмотра",
				// "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
			);
			$PRODUCT_ID = $el->Add($arLoadProductArray);	
				
		}
	}

	// создаем нотификацию
	$el = new CIBlockElement;
	$PROP = array();
	$PROP["USER"] = $_GET["ID"]; 
	// echo $date_active=date('d-m-Y H:i:s');
	$date_active=ConvertTimeStamp(time(),"FULL");


	$arLoadProductArray = Array(
		"MODIFIED_BY"    => $USER->GetID(),
		"IBLOCK_SECTION_ID" => false,        
		"IBLOCK_ID"      => 4,
		"NAME"           => "Вам назначены цели",
		"ACTIVE"         => "Y",
		"PROPERTY_VALUES"=> $PROP,
		"DATE_ACTIVE_FROM"=> $date_active
	);
	$PRODUCT_ID = $el->Add($arLoadProductArray);

}



	$arSelect = Array("ID", "NAME", "PROPERTY_READY");
	$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "PROPERTY_USER"=>$_GET["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>100), $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arResult["ITEMS"][]=$arFields;
		if ($arFields["PROPERTY_READY_VALUE"]=='Y') 
			{
				$goalsready=$goalsready+1;
			}
			else 
			{
				$goalsnotready=$goalsnotready+1;
			}		
	}
	$arResult["GOALS_READY"]=$goalsready;
	$arResult["GOALS_NOT_READY"]=$goalsnotready;

$this->includeComponentTemplate();
?>
