<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
// echo '<pre>';
// print_r($arResult);
// echo '</pre>';

$rsUser = CUser::GetByID($_GET["ID"]);
$arUser = $rsUser->Fetch();
?>

<!-- <button class="btn btn-primary mb-1" type="submit">К сотрудникам</button> -->

<a href="/hr/goals/"><button class="btn btn-icon btn-icon-start btn-outline-primary mb-1" type="button">
   <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="acorn-icons acorn-icons-chevron-left undefined"><path d="M13 16L7.35355 10.3536C7.15829 10.1583 7.15829 9.84171 7.35355 9.64645L13 4"></path></svg>
   &nbsp;&nbsp;&nbsp;<span>к списку сотрудников</span>
</button></a>

<br><br>

<div class="row">
<div class="col">

   <div class="card">
   <div class="card-body">
   <div>
      <? echo '<img src="'.CFile::GetPath($arUser["PERSONAL_PHOTO"]).'" width="50">';?>
   </div>
   <div>
   <?
   // echo "<pre>"; print_r($arUser); echo "</pre>";

   echo $arUser["NAME"].' ';
   echo $arUser["LAST_NAME"];
   ?>
   </div>
   </div>
   </div>

</div>
<div class="col" style="padding-top:35px;">
   Цели выполнены на 
   <div class="display-6 text-primary"><? 
   if ($arResult["GOALS_READY"]) {
      $proz=round($arResult["GOALS_READY"]*100/($arResult["GOALS_READY"]+$arResult["GOALS_NOT_READY"]),2);
   }
   else {
      $proz=0;
   }
   echo $proz;
   ?>%</div>
   <?
   if($arResult["GOALS_READY"]) 
      {
         $goalsready=$arResult["GOALS_READY"];
      } else {
         $goalsready=0;
      }

   ?>
   <b><? echo $goalsready; ?> из <? echo ($arResult["GOALS_READY"]+$arResult["GOALS_NOT_READY"]); ?></b>

</div>
</div>

<br>

<form method="post">
<h1 class="display-4">Список целей:</h1>
<div class="card">
<div class="card-body" style="display:block">

<?
		$i=0;
      $arSelect = Array("ID", "NAME", "PROPERTY_READY");
		$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "PROPERTY_USER"=>$_GET["ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>100), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
         $i=$i+1;

         // echo '<pre>';
         // print_r($arFields);
         // echo '</pre>';

         echo '<div style="width:100%" class="input-group">';
         if ($arFields["PROPERTY_READY_VALUE"]=='Y') echo '<span class="input-group-text" id="basic-addon1">Цель выполнена</span>';
         echo '<input class="form-control" name="f'.$i.'" value="'.$arFields["NAME"].'"></div>';
         //echo '<div style="width:100%"><input class="form-control" name="f2" value="'.$arResult["ITEMS"][1]["NAME"].'"></div>';

		}	

      $ostatok=5-$i;
      for ($j=1;$j<=$ostatok;$j++) {
         echo '<div style="width:100%" class="input-group">';
         echo '<input class="form-control" name="f'.($j+$i).'" value=""></div>';
      }

?>      


   <?/*
   <div style="width:100%"><input class="form-control" name="f3" value="<? echo $arResult["ITEMS"][2]["NAME"]; ?>"></div>
   <div style="width:100%"><input class="form-control" name="f4" value="<? echo $arResult["ITEMS"][3]["NAME"]; ?>"></div>
   <div style="width:100%"><input class="form-control" name="f5" value="<? echo $arResult["ITEMS"][4]["NAME"]; ?>"></div>
   */?>

</div>
</div>

<br>

<button class="btn btn-primary mb-1" type="submit">Сформировать цели</button>
</form>
<?



?>