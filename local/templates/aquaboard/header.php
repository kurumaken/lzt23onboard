﻿<!DOCTYPE html>
<html lang="en" data-footer="true">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta name="description" content="" />

    <?
    $APPLICATION->ShowHead();
    ?>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">



    <!-- Favicon Tags End -->
    <!-- Font Tags Start -->
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;700&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH;?>/font/CS-Interface/style.css" />
    <!-- Font Tags End -->
    <!-- Vendor Styles Start -->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH;?>/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH;?>/css/vendor/OverlayScrollbars.min.css" />

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH;?>/css/vendor/glide.core.min.css" />

    <!-- Vendor Styles End -->
    <!-- Template Base Styles Start -->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH;?>/css/styles.css" />
    <!-- Template Base Styles End -->

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH;?>/css/main.css" />
    <script src="<?=SITE_TEMPLATE_PATH;?>/js/base/loader.js"></script>
  </head>

  <body class="rtl">
    <div id="root">
      <div id="nav" class="nav-container d-flex">
        <div class="nav-content d-flex">
          <!-- Logo Start -->
          <div class="logo position-relative">
            <a href="/" style="color:#fff;font-size:20px;">
              <!-- Logo can be added directly -->
              <!-- <img src="img/logo/logo-white.svg" alt="logo" /> -->

              <!-- Or added via css to provide different ones for different color themes -->
              <?/*<div class="img"></div>*/?>
              AquaBoard
            </a>
          </div>
          <!-- Logo End -->

          <!-- User Menu Start -->
          
          
            

<?
if ($USER->isAuthorized()) {
?><div class="user-container d-flex">
<?$APPLICATION->IncludeComponent(
	"aqua:sidebar.ava",
	"",
Array()
);?>
</div>
<?
}
?>



            <?/*
            <div class="dropdown-menu dropdown-menu-end user-menu wide">
              <div class="row mb-3 ms-0 me-0">
                <div class="col-12 ps-1 mb-2">
                  <div class="text-extra-small text-primary">ACCOUNT</div>
                </div>
                <div class="col-6 ps-1 pe-1">
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">User Info</a>
                    </li>
                    <li>
                      <a href="#">Preferences</a>
                    </li>
                    <li>
                      <a href="#">Calendar</a>
                    </li>
                  </ul>
                </div>
                <div class="col-6 pe-1 ps-1">
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">Security</a>
                    </li>
                    <li>
                      <a href="#">Billing</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row mb-1 ms-0 me-0">
                <div class="col-12 p-1 mb-2 pt-2">
                  <div class="text-extra-small text-primary">APPLICATION</div>
                </div>
                <div class="col-6 ps-1 pe-1">
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">Themes</a>
                    </li>
                    <li>
                      <a href="#">Language</a>
                    </li>
                  </ul>
                </div>
                <div class="col-6 pe-1 ps-1">
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">Devices</a>
                    </li>
                    <li>
                      <a href="#">Storage</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row mb-1 ms-0 me-0">
                <div class="col-12 p-1 mb-3 pt-3">
                  <div class="separator-light"></div>
                </div>
                <div class="col-6 ps-1 pe-1">
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">
                        <i data-acorn-icon="help" class="me-2" data-acorn-size="17"></i>
                        <span class="align-middle">Help</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i data-acorn-icon="file-text" class="me-2" data-acorn-size="17"></i>
                        <span class="align-middle">Docs</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="col-6 pe-1 ps-1">
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">
                        <i data-acorn-icon="gear" class="me-2" data-acorn-size="17"></i>
                        <span class="align-middle">Settings</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i data-acorn-icon="logout" class="me-2" data-acorn-size="17"></i>
                        <span class="align-middle">Logout</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            */?>
          
<?
if ($USER->isAuthorized()) {
?>
          <ul class="list-unstyled list-inline text-center menu-icons">
            <li class="list-inline-item">
              <a href="/personal/profile/">
                <i data-acorn-icon="gear" data-acorn-size="18"></i>
              </a>
            </li>

            <li class="list-inline-item">
              <a href="/auth/logout/">
                <i data-acorn-icon="logout" data-acorn-size="18"></i>
              </a>
            </li>
          </ul>             
<?
}
else {
  echo '<div style="padding:10px;text-align:center">Сервис Онбоардинга. Решение в рамках Хакатона 2023.</div>';  
}
?>          


          <div class="menu-container flex-grow-1">
          <ul id="menu" class="menu">


<?




if(
  in_array(11,$USER->GetUserGroupArray()) OR 
  in_array(12,$USER->GetUserGroupArray())
) {

?>


          <li>      
            <a href="/personal/plan/" data-href="#1">
                  <i data-acorn-icon="home-garage" class="icon" data-acorn-size="18"></i>
                  <span class="label">План адаптации</span>
            </a>
          </li>  

          <li>      
            <a href="/personal/notifications/" data-href="#1">
                  <i data-acorn-icon="email" class="icon" data-acorn-size="18"></i>
                  <span class="label">Нотификации</span>
            </a>
          </li>  


          <li>      
            <a href="/personal/mygoals/" data-href="#1">
                  <i data-acorn-icon="star" class="icon" data-acorn-size="18"></i>
                  <span class="label">Мои цели</span>
            </a>
          </li>        
          
          <li>      
            <a href="/personal/mytests/" data-href="#1">
                  <i data-acorn-icon="notebook-1" class="icon" data-acorn-size="18"></i>
                  <span class="label">Мои курсы</span>
            </a>
          </li>   
<?
  
}

if(in_array(9,$USER->GetUserGroupArray())) {

?>

           <!-- <li>      
            <a href="/hr/plans/" data-href="#2">
                  <i data-acorn-icon="home" class="icon" data-acorn-size="18"></i>
                  <span class="label">Планы адаптации</span>
            </a>
           </li>  -->

          <li>      
            <a href="/personal/notificationsto/" data-href="#1">
                  <i data-acorn-icon="email" class="icon" data-acorn-size="18"></i>
                  <span class="label">Нотификации</span>
            </a>
          </li>  


          <li>      
            <a href="/hr/logs/" data-href="#1">
                  <i data-acorn-icon="wallet" class="icon" data-acorn-size="18"></i>
                  <span class="label">Логирование</span>
            </a>
          </li>            


          <li>      
            <a href="/hr/goals/" data-href="#2">
                  <i data-acorn-icon="star" class="icon" data-acorn-size="18"></i>
                  <span class="label">Цели сотрудникам</span>
            </a>
           </li> 
          <li>      
            <a href="/hr/analytics/" data-href="#2">
                  <i data-acorn-icon="activity" class="icon" data-acorn-size="18"></i>
                  <span class="label">Аналитика</span>
            </a>
           </li>  
<?
  
}

?>

<?

if ($USER->isAuthorized()) {
echo '  
          <li>      
            <a href="/company/docs/" data-href="#2">
                  <i data-acorn-icon="note" class="icon" data-acorn-size="18"></i>
                  <span class="label">Документы</span>
            </a>
          </li>


          <li>      
            <a href="/company/empl/" data-href="#2">
                  <i data-acorn-icon="user" class="icon" data-acorn-size="18"></i>
                  <span class="label">Сотрудники</span>
            </a>
          </li>  


          <li>      
            <a href="/company/support/" data-href="#2">
                  <i data-acorn-icon="help" class="icon" data-acorn-size="18"></i>
                  <span class="label">Техподдержка</span>
            </a>
          </li>';
} else {
?>
          <li>      
            <a href="/auth/" data-href="#2">
                  <i data-acorn-icon="key" class="icon" data-acorn-size="18"></i>
                  <span class="label">Авторизоваться</span>
            </a>
          </li>
<?  
}

?>                  



          

          


            <?/*
              <!--li>
                <a href="#dashboards" data-href="Dashboards.Patient.html">
                  <i data-acorn-icon="home-garage" class="icon" data-acorn-size="18"></i>
                  <span class="label">Dashboards</span>
                </a>
                <ul id="dashboards">
                  <li>
                    <a href="Dashboards.Patient.html">
                      <span class="label">Patient</span>
                    </a>
                  </li>
                  <li>
                    <a href="Dashboards.Doctor.html">
                      <span class="label">Doctor</span>
                    </a>
                  </li>
                </ul>
              </li-->
              <!--li>
                <a href="Appointments.html">
                  <i data-acorn-icon="calendar" class="icon" data-acorn-size="18"></i>
                  <span class="label">Appointments</span>
                </a>
              </li-->
              <!--li>
                <a href="Results.html">
                  <i data-acorn-icon="form-check" class="icon" data-acorn-size="18"></i>
                  <span class="label">Results</span>
                </a>
              </li>
              <li>
                <a href="Prescriptions.html">
                  <i data-acorn-icon="inbox" class="icon" data-acorn-size="18"></i>
                  <span class="label">Prescriptions</span>
                </a>
              </li>
              <li>
                <a href="Doctors.html">
                  <i data-acorn-icon="health" class="icon" data-acorn-size="18"></i>
                  <span class="label">Doctors</span>
                </a>
              </li>
              <li>
                <a href="Consult.html">
                  <i data-acorn-icon="messages" class="icon" data-acorn-size="18"></i>
                  <span class="label">Consult</span>
                </a>
              </li>
              <li>
                <a href="Guidebook.html">
                  <i data-acorn-icon="book-open" class="icon" data-acorn-size="18"></i>
                  <span class="label">Guidebook</span>
                </a>
              </li>
              <li>
                <a href="Articles.html">
                  <i data-acorn-icon="book" class="icon" data-acorn-size="18"></i>
                  <span class="label">Articles</span>
                </a>
              </li-->
              <!--li>
                <a href="Settings.html">
                  <i data-acorn-icon="gear" class="icon" data-acorn-size="18"></i>
                  <span class="label">Settings</span>
                </a>
              </li-->
              */?>
            </ul>



          
          </div>
          <!-- Menu End -->
     
      

          <!-- Mobile Buttons Start -->
          <div class="mobile-buttons-container">
            <!-- Menu Button Start -->
            <a href="#" id="mobileMenuButton" class="menu-button">
              <i data-acorn-icon="menu"></i>
            </a>
            <!-- Menu Button End -->
          </div>
          <!-- Mobile Buttons End -->
        </div>
        <div class="nav-shadow"></div>
      </div>

      <main>
        <div class="container">
          <!-- Title and Top Buttons Start -->
          <div class="page-title-container">
            <div class="row">
              <!-- Title Start -->
              <div class="col-12 col-md-7">
                <span class="align-middle text-muted d-inline-block lh-1 pb-2 pt-2 text-small">Сервис адаптации сотрудников</span>
                <!-- Добро пожаловать! -->
                <h1 class="mb-0 pb-0 display-4" id="title"><? $APPLICATION->ShowTitle(true);?></h1>
              </div>
              <!-- Title End -->
            </div>
          </div>
          <!-- Title and Top Buttons End -->

