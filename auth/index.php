<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");

if (is_string($_REQUEST["backurl"]) && strpos($_REQUEST["backurl"], "/") === 0)
{
	LocalRedirect($_REQUEST["backurl"]);
}



//HR
if(in_array(9,$USER->GetUserGroupArray())) {
	// authirizedlog();
	if ($USER->isAuthorized()) {
			//to log
			$el = new CIBlockElement;
			$PROP = array();
			$PROP["USER"] = $USER->GetID(); 
			// echo $date_active=date('d-m-Y H:i:s');
			$date_active=ConvertTimeStamp(time(),"FULL");


			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,        
				"IBLOCK_ID"      => 5,
				"NAME"           => 'HR авторизовался',
				"ACTIVE"         => "Y",
				"PROPERTY_VALUES"=> $PROP,
				"DATE_ACTIVE_FROM"=> $date_active
			);
			$PRODUCT_ID = $el->Add($arLoadProductArray);	
	}
	LocalRedirect('/hr/analytics/');
}

//HR
if(in_array(11,$USER->GetUserGroupArray())) {
	// authirizedlog();
	if ($USER->isAuthorized()) {
			//to log
			$el = new CIBlockElement;
			$PROP = array();
			$PROP["USER"] = $USER->GetID(); 
			// echo $date_active=date('d-m-Y H:i:s');
			$date_active=ConvertTimeStamp(time(),"FULL");


			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,        
				"IBLOCK_ID"      => 5,
				"NAME"           => 'Программист авторизовался',
				"ACTIVE"         => "Y",
				"PROPERTY_VALUES"=> $PROP,
				"DATE_ACTIVE_FROM"=> $date_active
			);
			$PRODUCT_ID = $el->Add($arLoadProductArray);	
	}	
	LocalRedirect('/personal/plan/');
}

//HR
if(in_array(12,$USER->GetUserGroupArray())) {
	// authirizedlog();
	if ($USER->isAuthorized()) {
			//to log
			$el = new CIBlockElement;
			$PROP = array();
			$PROP["USER"] = $USER->GetID(); 
			// echo $date_active=date('d-m-Y H:i:s');
			$date_active=ConvertTimeStamp(time(),"FULL");


			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,        
				"IBLOCK_ID"      => 5,
				"NAME"           => 'Дизайнер авторизовался',
				"ACTIVE"         => "Y",
				"PROPERTY_VALUES"=> $PROP,
				"DATE_ACTIVE_FROM"=> $date_active
			);
			$PRODUCT_ID = $el->Add($arLoadProductArray);	
	}	
	LocalRedirect('/personal/plan/');
}





?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>